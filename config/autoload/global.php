<?php

namespace Application;

const HOUSE_OF_CARDS = [
    0  => 'A',
    1  => '2',
    2  => '3',
    3  => '4',
    4  => '5',
    5  => '6',
    6  => '7',
    7  => '8',
    8  => '9',
    9  => '10',
    10 => 'J',
    11 => 'Q',
    12 => 'K',
];

const SUITS = [
    0 => 'Spades',
    1 => 'Clubs',
    2 => 'Diamonds',
    3 => 'Hearts',
];

const SUIT_ICONS = [
    0 => 'far fa-spade',
    1 => 'far fa-club',
    2 => 'far fa-diamond',
    3 => 'far fa-heart',
];

return [];