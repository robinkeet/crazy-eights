# Crazy Eights game in PHP

How to get it:

* clone this repo with: `git clone git@bitbucket.org:robinkeet/crazy-eights.git`
* run: `composer install` or `composer install --no-dev` (no development packages)

Make sure you have it hosted somewhere, e.g.:
 
Xampp - in Apache's `httpd-vhosts.confg` file on Windows:

```apacheconfig
<VirtualHost *:80>
	DocumentRoot "C:/xampp/htdocs/crazy-eights"
	ServerName crazy-eights.loc
	
	SetEnv APPLICATION_ENV "development"
  
	<Directory C:/xampp/htdocs/crazy-eights>
		DirectoryIndex index.php index.html
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>
</VirtualHost>
```

And add a line to the Windows hosts file:

```
127.0.0.1	crazy-eights.loc
```

### Additional notes

* Crazy Eights game: module/Home/ - contains whole module including Entities and game logic
* Crazy Eights theme: module/Theme/ - contains the layout for the project
* Resources/assets: public/ - contains the JS/CSS/ico and .htaccess and project entry point
