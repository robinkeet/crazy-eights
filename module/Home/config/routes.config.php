<?php

namespace Home;

use Home\Controller\HomeController;
use Zend\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => HomeController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
];
