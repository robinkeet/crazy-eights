<?php

namespace Home;

use Home\Controller\HomeController;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers'  => [
        'factories' => [
            HomeController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'controller_map'      => [
            HomeController::class => 'home',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
