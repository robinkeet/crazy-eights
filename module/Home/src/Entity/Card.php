<?php

namespace Home\Entity;

use Exception;
use Keet\Mvc\Entity\AbstractEntity;
use const Application\HOUSE_OF_CARDS;
use const Application\SUIT_ICONS;
use const Application\SUITS;

class Card extends AbstractEntity
{
    /**
     * @var int
     */
    protected $rank;

    /**
     * @var int
     */
    protected $suit;

    /**
     * @return string
     */
    public function fullName()
    {
        return HOUSE_OF_CARDS[$this->getRank()] . ' of ' . SUITS[$this->getSuit()];
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return SUIT_ICONS[$this->getSuit()];
    }

    /**
     * @return string
     */
    public function shortName()
    {
        return substr(SUITS[$this->getSuit()], 0, 1) . HOUSE_OF_CARDS[$this->getRank()];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '<i class="' . SUIT_ICONS[$this->getSuit()] . '"></i>' . HOUSE_OF_CARDS[$this->getRank()];
    }

    /**
     * @param Card $card
     *
     * @return bool
     * @throws Exception
     */
    public function isEqualRank(Card $card)
    {
        if ($card === $this) {
            throw new Exception('Trying to compare card to itself.');
        }

        return $this->getRank() === $card->getRank();
    }

    /**
     * @param Card $card
     *
     * @return bool
     * @throws Exception
     */
    public function isEqualSuite(Card $card)
    {
        if ($card === $this) {
            throw new Exception('Trying to compare card to itself.');
        }

        return $this->getSuit() === $card->getSuit();
    }

    /**
     * @return int
     */
    public function getRank() : int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     *
     * @return Card
     */
    public function setRank(int $rank) : Card
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * @return int
     */
    public function getSuit() : int
    {
        return $this->suit;
    }

    /**
     * @param int $suit
     *
     * @return Card
     */
    public function setSuit(int $suit) : Card
    {
        $this->suit = $suit;

        return $this;
    }

}