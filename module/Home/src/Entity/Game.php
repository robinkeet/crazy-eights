<?php

namespace Home\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Keet\Mvc\Entity\AbstractEntity;

class Game extends AbstractEntity
{
    /**
     * @var ArrayCollection|Card[]
     */
    protected $discardPile;

    /**
     * @var ArrayCollection|Card[]
     */
    protected $drawPile;

    /**
     * @var ArrayCollection|Player[]
     */
    protected $players;

    public function __construct()
    {
        $this->discardPile = new ArrayCollection();
        $this->drawPile = new ArrayCollection();
        $this->players = new ArrayCollection();
    }

    /**
     * @return null|Card
     */
    public function drawTopCard() : ?Card
    {
        return $this->getDrawPile()->first() ?: null;
    }

    /**
     * @param ArrayCollection $collection
     */
    public function shuffle(ArrayCollection &$collection) : void
    {
        $array = $collection->toArray();
        shuffle($array);

        $collection->clear();
        array_map(
            function ($item) use (&$collection) {
                $collection->add($item);
            },
            $array
        );
    }

    /**
     * @return ArrayCollection|Card[]
     */
    public function getDiscardPile() : ArrayCollection
    {
        return $this->discardPile;
    }

    /**
     * @param ArrayCollection $cards
     *
     * @return Game
     */
    public function addDiscardPile(ArrayCollection $cards) : Game
    {
        foreach ($cards as $card) {
            if (
                $card instanceof Card
                && ! $this->getDiscardPile()->contains($card)
            ) {
                $this->getDiscardPile()->add($card);
            }
        }

        return $this;
    }

    /**
     * @param ArrayCollection|Card[] $cards
     *
     * @return Game
     */
    public function removeDiscardPile(ArrayCollection $cards) : Game
    {
        foreach ($cards as $card) {
            if (
                $card instanceof Card
                && $this->getDiscardPile()->contains($card)
            ) {
                $this->getDiscardPile()->remove($card);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Card[]
     */
    public function getDrawPile() : ArrayCollection
    {
        return $this->drawPile;
    }

    /**
     * @param ArrayCollection $cards
     *
     * @return Game
     */
    public function addDrawPile(ArrayCollection $cards) : Game
    {
        foreach ($cards as $card) {
            if (
                $card instanceof Card
                && ! $this->getDrawPile()->contains($card)
            ) {
                $this->getDrawPile()->add($card);
            }
        }

        return $this;
    }

    /**
     * @param ArrayCollection|Card[] $cards
     *
     * @return Game
     */
    public function removeDrawPile(ArrayCollection $cards) : Game
    {
        foreach ($cards as $card) {
            if (
                $card instanceof Card
                && $this->getDrawPile()->contains($card)
            ) {
                $this->getDrawPile()->remove($card);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Player[]
     */
    public function getPlayers() : ArrayCollection
    {
        return $this->players;
    }

    /**
     * @param ArrayCollection|Player[] $players
     *
     * @return Game
     */
    public function addPlayers(ArrayCollection $players) : Game
    {
        foreach ($players as $player) {
            if (
                $player instanceof Player
                && ! $this->getPlayers()->contains($player)
            ) {
                $this->getPlayers()->add($player);
            }
        }

        return $this;
    }

    /**
     * @param ArrayCollection|Player[] $players
     *
     * @return Game
     */
    public function removePlayers(ArrayCollection $players) : Game
    {
        foreach ($players as $player) {
            if (
                $player instanceof Player
                && $this->getPlayers()->contains($player)
            ) {
                $this->getPlayers()->remove($player);
            }
        }

        return $this;
    }

}