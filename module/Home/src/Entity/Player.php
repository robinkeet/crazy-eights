<?php

namespace Home\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Keet\Mvc\Entity\AbstractEntity;

class Player extends AbstractEntity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Players' hand of Card's
     *
     * @var ArrayCollection|Card[]
     */
    protected $cards;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->getName();
    }

    /**
     * @return ArrayCollection|Card[]
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * @param ArrayCollection|Card[] $cards
     *
     * @return Player
     */
    public function addCards(ArrayCollection $cards) : Player
    {
        foreach ($cards as $card) {
            if ( ! $this->getCards()->contains($card)) {
                $this->getCards()->add($card);
            }
        }

        return $this;
    }

    /**
     * @param ArrayCollection|Card[] $cards
     *
     * @return Player
     */
    public function removeCards(ArrayCollection $cards) : Player
    {
        foreach ($cards as $card) {
            if ($this->getCards()->contains($card)) {
                $this->getCards()->remove($card);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Player
     */
    public function setName(string $name) : Player
    {
        $this->name = $name;

        return $this;
    }

}