<?php

namespace Home\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Exception;
use Home\Entity\Card;
use Home\Entity\Game;
use Home\Entity\Player;
use Keet\Mvc\Controller\AbstractActionController;
use const Application\HOUSE_OF_CARDS;
use const Application\SUITS;

class HomeController extends AbstractActionController
{
    public function indexAction()
    {
        // For Deck displaying only
        $suits = [];
        $criteria = new Criteria();

        $deck = $this->createDeck();

        // Loop through defined suits
        foreach (\Application\SUITS as $key => $suit) {
            // Place cards of Deck matching criteria (suit = a key from the SUITS) in array sub-key
            $suits[$key] = $deck->matching(
                $criteria->where($criteria::expr()->eq('suit', $key))
            );
        }

        // Now, let's play a game

        $players = new ArrayCollection();
        for ($i = 0; $i < 4; $i++) {
            $players->add(
                (new Player())
                    ->setName('Player' . (string) ($i + 1))
            );
        }

        $game = (new Game())
            ->addPlayers($players);

        $game->shuffle($deck);

        $this->dealHands($players, $deck);

        $afterDealing = clone $deck; // for display purposes only

        array_map(
            function ($card) use (&$game) {
                $game->addDrawPile(new ArrayCollection([$card]));
            },
            $deck->toArray()
        );

        $startCard = $game->drawTopCard();
        $game->addDiscardPile(new ArrayCollection([$startCard]));
        $game->getDrawPile()->removeElement($startCard);

        // do turns until we have a winner
        $moves = [];

        // Loop until win condition reached
        while (true) {
            /** @var array|Player[] $players */
            $players = $game->getPlayers()->toArray();

            /** @var Card $lastCard */
            $lastCard = $game->getDiscardPile()->last();

            foreach ($players as $player) {
                // Find cards allowed based on suit or rank
                $allowedCards = $player->getCards()->matching(
                    $criteria
                        ->where($criteria::expr()->eq('suit', $lastCard->getSuit()))
                        ->orWhere($criteria::expr()->eq('rank', $lastCard->getRank()))
                );

                // No cards allowed to play. Draw an additional card
                if ($allowedCards->count() === 0) {
                    $drawCard = $game->drawTopCard();
                    if (is_null($drawCard)) {
                        $moves[] = 'Draw pile empty. Stopping game.';
                        break 2; // Stop condition, draw pile empty.
                    }

                    $player->addCards(new ArrayCollection([$drawCard]));
                    $game->getDrawPile()->removeElement($drawCard);
                    $moves[] = $player . ' does not have a suitable card, draws card ' . $drawCard;
                    unset($drawCard);
                    continue 1; // Continue next turn
                }

                // Play a card
                // (Always playing first card. Could randomize card to play from allowed cards if more than one...)
                if ($allowedCards->count() > 0) {
                    $playCard = $allowedCards->first();
                    $game->addDiscardPile(new ArrayCollection([$playCard]));
                    $player->getCards()->removeElement($playCard);

                    // Update lastCard
                    $lastCard = $playCard;

                    // Register move
                    $moves[] = $player . ' plays ' . $playCard;

                    // Clear card to play
                    unset($playCard);

                    // Required to announce single remaining card
                    if ($player->getCards()->count() === 1) {
                        $moves[] = $player . ' only as 1 card remaining!';
                    }

                    // Win condition
                    if ($player->getCards()->count() === 0) {
                        $moves[] = $player . ' has won!';
                        break 2; // Break foreach & while loops
                    }
                }
            }

            if (count($moves) >= 100) {
                $moves[] = 'Stopping game, 100 moves reached.';
                break 1; // Stop condition, we've reached 100 moves
            }

            // If winner not set / loops not broken, continue playing
            reset($players);
        }

        return [
            'suits'        => $suits, // Not to be used for anything but display the Deck
            'players'      => $players,
            'afterDealing' => $afterDealing,
            'startCard'    => $game->getDiscardPile()->first(),
            'moves'        => $moves,
        ];
    }

    /**
     * @param ArrayCollection|Player[] $players
     * @param ArrayCollection|Card[]   $deck
     *
     * Deals hands of cards to received players. Params are reference, Card's dealt are removed from received Deck
     * and given to Players.
     */
    protected function dealHands(ArrayCollection $players, ArrayCollection &$deck)
    {
        foreach ($players as $player) {
            while ($player->getCards()->count() !== 7) {
                $card = $deck->first();
                $deck->removeElement($card);
                $player->addCards(new ArrayCollection([$card]));
            }
        }
    }

    /**
     * Creates a Deck of Cards
     *
     * @return ArrayCollection
     */
    protected function createDeck() : ArrayCollection
    {
        $deck = new ArrayCollection();

        foreach (SUITS as $suit => $label) {
            foreach (HOUSE_OF_CARDS as $rank => $name) {
                $deck
                    ->add(
                        (new Card())
                            ->setSuit($suit)
                            ->setRank($rank)
                    );
            }
        }

        return $deck;
    }

}